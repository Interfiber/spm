FROM rust:1.59-bullseye as builder

RUN apt-get install -y libsqlite3-dev

WORKDIR /spm

COPY . .

RUN cargo build --release
RUN cargo install diesel_cli --no-default-features --features "sqlite"
RUN /usr/local/cargo/bin/diesel migration run


FROM rust:1.59-bullseye


RUN mkdir /spm
COPY --from=builder /spm/target/release/spm /spm/spm

COPY --from=builder /spm/spm.db /spm/spm.db

COPY --from=builder /spm/.env /spm/.env

WORKDIR /spm

EXPOSE 8080

CMD ["/spm/spm"]
