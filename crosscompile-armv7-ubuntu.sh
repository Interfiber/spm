#!/usr/bin/env bash
# update repos
apt update

# install build requirments
apt install -y git curl make gcc gcc-multilib-arm-linux-gnueabihf wget

# clone repo
git clone --recursive https://gitlab.com/Interfiber/spm
cd spm

# rustup setup
curl https://sh.rustup.rs -sSf | sh
source ~/.cargo/env

# add build targets
rustup target add armv7-unknown-linux-gnueabihf

# add files
echo "[target.armv7-unknown-linux-gnueabihf]
linker = \"arm-linux-gnueabihf-gcc\"" >> ~/.cargo/config

# build sqlite3
wget http://www.sqlite.org/2017/sqlite-autoconf-3160200.tar.gz
tar xvf sqlite-autoconf-3160200.tar.gz
cd sqlite-autoconf-3160200/

./configure --host=arm-linux CC=$(which arm-linux-gnueabihf-gcc)
make
make install
cd ..
# build release
cargo build --target=armv7-unknown-linux-gnueabihf --release
# done
echo "Built"
