
try {
    let vaultCache = JSON.parse(sessionStorage.getItem("vaultCache"));
    if (vaultCache["items"] == undefined){
        vaultCache.items = [];
        sessionStorage.setItem("vaultCache", JSON.stringify(vaultCache));
    }
} catch (err) {
    console.log("Invalid cache, creating empty one");
    sessionStorage.setItem("vaultCache", JSON.stringify({
        "items": [],
        "vault_version": "1"
    }))
}


async function signup(){
    let email = document.querySelector(".emailInput");
    let password = document.querySelector(".passwordInput");
    if (email.value == "" || password.value == ""){
        alert("Please fill out the form!");
    } else {
        let signupFetch = await fetch("/api/v1/users/register", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email.value,
                password: password.value
            })
        });
        try {
            let response = await signupFetch.json();
            console.log(response);
            if (response.error == true){
                console.log("--- Error: Server returned account creation error ---");
                console.log("Sending alert...");
                alert(response.msg);
            } else {
                alert("Created account, please signin");
            }
        } catch(error) {
            console.log("--- Error: Server returned invalid response ---");
            console.log("Error log: " + error);
            console.log("Sending alert....");
            alert("Server returned invalid response, open dev console for more info");
            return;
        }

    }
}

async function login(){
    let email = document.querySelector(".emailInput");
    let password = document.querySelector(".passwordInput");
    if (email.value == "" || password.value == ""){
        alert("Please fill out the form!");
    } else {
        let vaultFetch = await fetch("/api/v1/vault/get_user_vault", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email.value,
                password: password.value
            })
        });
        try {
            let response = await vaultFetch.json();
            console.log(response);
            if (response.error == true){
                console.log("--- Error: Server returned error ---");
                console.log("Sending alert...");
                alert(response.msg);
            } else {
                // set cache
                sessionStorage.setItem("vaultCache", JSON.stringify(response.vault));
                sessionStorage.setItem("emailCache", email.value);
                sessionStorage.setItem("passwordCache", password.value);
                window.location.href = "/vault.html";
            }
        } catch(error) {
            console.log("--- Error: Server returned invalid response ---");
            console.log("Error log: " + error);
            console.log("Sending alert....");
            alert("Server returned invalid response, open dev console for more info");
            return;
        }
    }
}

async function checkAuth(){
    // just check for sessionStorage values, the api will throw errors later
    if (sessionStorage.getItem("emailCache") == null) {
        alert("Please login first!");
        window.location.href = "/";
    } else if (sessionStorage.getItem("passwordCache") == null){
        alert("Please login first!");
        window.location.href = "/"; 
    } else if (sessionStorage.getItem("vaultCache") == null) {
        alert("Please login first!");
        window.location.href = "/"; 
    }
}

function copyText(text) {
    // document.querySelector(".copyData").innerHTML = text;
    // document.querySelector(".copyData").focus();
    // document.querySelector(".copyData").select();
    if (navigator.clipboard == undefined){
        document.querySelector(".alertTitle").innerHTML = "Failed to copy!";
        document.querySelector(".alertBody").innerHTML = `Failed to copy text, this site is not running HTTPS!\nInstead copy the password from here: ${text}`;
        document.querySelector(".alert").style.display = "";
    }
    navigator.clipboard.writeText(text);
    alert("Copied text to clipboard");
}

async function syncVault(){
    console.log("Syncing vault items to server...");
    let syncFetch = await fetch("/api/v1/vault/upload_new_vault", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            email: sessionStorage.getItem("emailCache"),
            password: sessionStorage.getItem("passwordCache"),
            vault_data: JSON.parse(sessionStorage.getItem("vaultCache"))
        })
    });

    try {
        let syncReponse = await syncFetch.json();
        if (syncReponse.error == true){
            alert(syncReponse.msg);
        }
    } catch (err) {
        console.log("Error during sync: " + err);
    }
}


async function deleteItem(){
    let boxes = document.querySelectorAll(".checkboxSelect");
    if (boxes != null){
        boxes.forEach((box) => {
            if (box.checked) {
                console.log("removing item");
                let id = box.dataset.vaultId;
                let vaultCache = JSON.parse(sessionStorage.getItem("vaultCache")).items;
                let index = null;
                console.log(vaultCache);
                for (let i = 0; i < vaultCache.length; i++) {
                    let item = vaultCache[i];
                    if (item.id == id){
                        index = i;
                        break;
                    }
                }
                console.log(index);
                if (index == null){
                    alert("Failed to find item in db");
                } else {
                    vaultCache.splice(index, 1);
                    sessionStorage.setItem("vaultCache", JSON.stringify({
                        "items": vaultCache
                    }));
                    syncVault();
                    alert("Deleted item");
                    window.location.reload();
                }
            }
        });
    }
}

async function renderVault(){
    let vault = JSON.parse(sessionStorage.getItem("vaultCache"));
    console.log(vault);
    let body = document.querySelector(".passwordBody");
    vault.items.forEach((item) => {
        if (item.type != "password") {
            console.log("skipping");
        } else {
            let tr = document.createElement("tr");
            tr.classList.add(`${item.id}_item`);
            body.appendChild(tr);

            let tdSelect = document.createElement("td");
            let tdCheck = document.createElement("input");
            tdCheck.type = "checkbox";
            tdCheck.dataset.vaultId = item.id;
            tdCheck.classList.add("checkboxSelect");
            tdSelect.appendChild(tdCheck);

            let tdTitle = document.createElement("td");
            tdTitle.innerHTML = item.name;
            let tdSite = document.createElement("td");
            tdSite.innerHTML = `<a href="${item.login.website}">${item.login.website}</a>`;
            let tdCopy = document.createElement("td");
            let tdCopyButton = document.createElement("button");
            tdCopyButton.classList.add("button");
            tdCopyButton.classList.add("is-link");
            tdCopyButton.innerHTML = "Copy password";
            tdCopyButton.onclick = function (){
                copyText(item.login.password);
            }
            tdCopy.appendChild(tdCopyButton);
            let tdEdit = document.createElement("td");
            let tdEditButton = document.createElement("button");
            tdEditButton.innerHTML = "Edit";
            tdEditButton.classList.add("button");
            tdEditButton.classList.add("is-primary");
            tdEditButton.onclick = function () {
                window.location.href = `/edititem.html?id=${item.id}`;
            }
            tdEdit.appendChild(tdEditButton);
            tr.appendChild(tdSelect);
            tr.appendChild(tdTitle);
            tr.appendChild(tdSite);
            tr.appendChild(tdCopy);
            tr.appendChild(tdEdit);
        }
    });
}

async function getUUID(){
    let uuidFetch = await fetch("/api/v1/uuid/generate");
    let uuidJson = await uuidFetch.json();
    return uuidJson.uuid;
}

async function createItem(){
    let name = document.querySelector(".nameInput").value;
    let username = document.querySelector(".userInput").value;
    let password = document.querySelector(".passwordInput").value;
    let website = document.querySelector(".siteInput").value;
    if (username == "" || password == "" || name == "") {
        alert("Please fill out username, password, and name!");
    } else {
        let vaultCache = JSON.parse(sessionStorage.getItem("vaultCache")).items;
        let uuid = await getUUID();
        vaultCache.push({
            "name": name,
            "type": "password",
            "id": uuid,
            "login": {
                "username": username,
                "password": password,
                "website": website
            },
            "note": ""
        });
        sessionStorage.setItem("vaultCache", JSON.stringify({
            "items": vaultCache
        }));
        syncVault();
        alert("Created item");
    }
}

async function updateItem(){
    let searchParams = new URLSearchParams(window.location.search);
    let id = searchParams.get("id");
    if (id == null){
        alert("Invalid id");
        window.location.href = "/vault.html";
        return;
    }
    let name = document.querySelector(".nameInput").value;
    let username = document.querySelector(".userInput").value;
    let password = document.querySelector(".passwordInput").value;
    let website = document.querySelector(".siteInput").value;
    if (username == "" || password == "" || name == "") {
        alert("Please fill out username, password, and name!");
    } else {
        let vaultCache = JSON.parse(sessionStorage.getItem("vaultCache")).items;
        // find the item in the local cache
        vaultCache.forEach((item) => {
            if (item.id == id) {
                // update item
                item.name = name;
                item.login.username = username;
                item.login.password = password;
                item.login.website = website;
                return;
            }
        })
        // update cache
        sessionStorage.setItem("vaultCache", JSON.stringify({
            "items": vaultCache
        }));
        syncVault();
        alert("Updated item");
    }
}

async function loadItemInfo(){
    let searchParams = new URLSearchParams(window.location.search);
    let id = searchParams.get("id");
    if (id == null){
        alert("Invalid id");
        window.location.href = "/vault.html";
        return;
    }
    let name = document.querySelector(".nameInput");
    let username = document.querySelector(".userInput");
    let password = document.querySelector(".passwordInput");
    let website = document.querySelector(".siteInput");

    let vaultCache = JSON.parse(sessionStorage.getItem("vaultCache")).items;

    vaultCache.forEach((item) => {
        if (item.id == id){
            name.value = item.name;
            username.value = item.login.username;
            password.value = item.login.password;
            website.value = item.login.website;
            return;
        }
    });
}

async function copyAndDecode(id){
    let vault = JSON.parse(sessionStorage.getItem("vaultCache"));
    vault.items.forEach(async (item) => {
        if (item.id == id && item.type == "totp"){
            // decode
            let decodeFetch = await fetch("/api/v1/vault/decode_totp", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    "totp_secret": item.login.secret
                })
            });
            let decodeData = await decodeFetch.json();
            console.log(decodeData);
            copyText(decodeData.totp_decoded);
        }
    });
}

async function renderTotpVault(){
    let vault = JSON.parse(sessionStorage.getItem("vaultCache"));
    console.log(vault);
    let body = document.querySelector(".totpBody");
    vault.items.forEach((item) => {
        if (item.type != "totp") {
            console.log("skipping");
        } else {
            let tr = document.createElement("tr");
            tr.classList.add(`${item.id}_item`);
            body.appendChild(tr);

            let tdSelect = document.createElement("td");
            let tdCheck = document.createElement("input");
            tdCheck.type = "checkbox";
            tdCheck.dataset.vaultId = item.id;
            tdCheck.classList.add("checkboxSelect");
            tdSelect.appendChild(tdCheck);

            let tdTitle = document.createElement("td");
            tdTitle.innerHTML = item.name;
            let tdCopy = document.createElement("td");
            let tdCopyButton = document.createElement("button");
            tdCopyButton.classList.add("button");
            tdCopyButton.classList.add("is-link");
            tdCopyButton.innerHTML = "Copy Code";
            tdCopyButton.onclick = function (){
                copyAndDecode(item.id);
            }
            tdCopy.appendChild(tdCopyButton);
            tr.appendChild(tdSelect);
            tr.appendChild(tdTitle);
            tr.appendChild(tdCopy);
        }
    });
}

async function createTotpItem(){
    let file = document.querySelector(".qrCodeUpload").files[0];
    const reader = new FileReader();
    reader.addEventListener("load", async () => {
        let url = reader.result;
        qrcode.callback = async function (data) {
            console.log(data);
            // parse the url
            let url = new URL(data);
            console.log(url.search);
            let secret = new URLSearchParams(url.search).get("secret");

            // add to database
            let addFetch = await fetch("/api/v1/vault/upload_new_totp", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    email: sessionStorage.getItem("emailCache"),
                    password: sessionStorage.getItem("passwordCache"),
                    totp_name: document.querySelector(".nameInput").value,
                    totp_secret: secret
                })
            });
            let addData = await addFetch.json();
            console.log(addData);
            // download the new vault onto the machine
            let vaultFetch = await fetch("/api/v1/vault/get_user_vault", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    email: sessionStorage.getItem("emailCache"),
                    password: sessionStorage.getItem("passwordCache")
                })
            });
            let vaultDownload = await vaultFetch.json();
            sessionStorage.setItem("vaultCache", JSON.stringify(vaultDownload.vault));
            alert("Added item");
        }
        qrcode.decode(url);
    });
    if (file) {
        reader.readAsDataURL(file);
    }
}