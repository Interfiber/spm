table! {
    users (id) {
        id -> Integer,
        email -> Text,
        password_hashed -> Text,
        vault -> Text,
    }
}
