use bcrypt::{DEFAULT_COST, hash, verify};
use magic_crypt::{new_magic_crypt, MagicCryptTrait};


pub fn hash_password(password: String) -> String {
    let hashed = hash(password, DEFAULT_COST).expect("Failed to hash password");    
    return hashed;
}

pub fn verify_password(password: String, hashed: String) -> bool {
    let check = verify(password, &hashed).expect("Failed to verify password");
    return check;
}

pub fn encrypt_data(password: String, data: String) -> String {
    let mc = new_magic_crypt!(password, 256);
    let encrypted = mc.encrypt_str_to_base64(data);
    return encrypted;
}

pub fn decrypt_data(password: String, data: String) -> String {
    let mc = new_magic_crypt!(password, 256);
    let decrypt = mc.decrypt_base64_to_string(&data).expect("Failed to decrypt");
    return decrypt;
}
