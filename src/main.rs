use actix_web::{get, post, web::Json, App, HttpResponse, HttpServer, Responder};
use actix_web_static_files::ResourceFiles;
use serde_json::json;
use enums::{VaultReadStatus, VaultUploadStatus, TotpUploadStatus};
use requests::{UserRegister, VaultUpload, UploadTotp, DecodeTotp};
#[macro_use] extern crate diesel;

mod requests;
mod db;
mod models;
mod schema;
mod security;
mod enums;
mod vault;

include!(concat!(env!("OUT_DIR"), "/generated.rs"));

#[get("/ping")]
async fn echo() -> impl Responder {
    HttpResponse::Ok().body("pong")
}

#[post("/api/v1/users/register")]
async fn register(user: Json<UserRegister>) -> impl Responder {
    let create_status = db::register_user_req(user);
    if create_status == enums::UserCreateStatus::EmailTaken {
        let response = requests::UserCreate {
            error: true,
            msg: "Email is already in use".to_string()
        };
        Json(response)
    } else {
        let response = requests::UserCreate {
            error: false,
            msg: "Created user in database".to_string()
        };
        Json(response)
    }
}

#[post("/api/v1/vault/get_user_vault")]
async fn get_vault(user: Json<UserRegister>) -> impl Responder {
    let vault = db::get_user_vault_req(user);
    match vault {
        Ok(data) => {
            return HttpResponse::Ok().body(json!({
                "error": false,
                "msg": "Got vault",
                "vault": data
            }).to_string());
        },
        Err(status) => {
            match status {
                VaultReadStatus::InvalidUser => {
                    return HttpResponse::Unauthorized().body(json!({
                        "error": true,
                        "msg": "Invalid User, the user may not exist or the password may be incorrect",
                        "vault": null
                    }).to_string());
                }
            }
        }
    }
}

#[post("/api/v1/vault/upload_new_vault")]
async fn upload_vault(req: Json<VaultUpload>) -> impl Responder {
    let sync_status = db::set_vault_req(req);
    match sync_status {
        VaultUploadStatus::InvalidUser => {
            return HttpResponse::Unauthorized().body(json!({
                "error": true,
                "msg": "Invalid User, the user may not exist or the password may be incorrect",
            }).to_string());
        },
        VaultUploadStatus::UploadedVault => {
            return HttpResponse::Ok().body(json!({
                "error": false,
                "msg": "Updated vault"
            }).to_string());
        }
    }
}

#[post("/api/v1/vault/upload_new_totp")]
async fn upload_totp(req: Json<UploadTotp>) -> impl Responder {
    let result = db::add_totp_req(req);
    match result {
        TotpUploadStatus::UploadedTotp => {
            return HttpResponse::Ok().body(json!({
                "error": false,
                "msg": "Uploaded totp to vault"
            }).to_string());
        },
        TotpUploadStatus::InvalidUser => {
            return HttpResponse::Unauthorized().body(json!({
                "error": true,
                "msg": "Invalid User, the user may not exists or the password may be incorrect"
            }).to_string());
        }
    }
}

#[post("/api/v1/vault/decode_totp")]
async fn decode_totp_req(req: Json<DecodeTotp>) -> impl Responder {
    let decoded = vault::decode_totp(req.totp_secret.clone());
    HttpResponse::Ok().body(json!({
        "error": false,
        "msg": "Decode totp",
        "totp_decoded": decoded
    }).to_string())
}

#[get("/api/v1/uuid/generate")]
async fn new_uuid() -> impl Responder {
    let generated_uuid = uuid::Uuid::new_v4();
    HttpResponse::Ok().body(json!({
        "error": false,
        "msg": "Generate UUID",
        "uuid": generated_uuid.to_string()
    }).to_string())
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!("Trying to start server on: 0.0.0.0:8080");
    HttpServer::new(|| {
        let generated = generate();
        App::new()
            .service(echo)
            .service(register)
            .service(get_vault)
            .service(upload_vault)
            .service(upload_totp)
            .service(decode_totp_req)
            .service(new_uuid)
            .service(ResourceFiles::new("/", generated))
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}
