use serde_json::json;
use crate::enums::VaultReadStatus;
use serde_json::Value;
use crate::requests::{UserRegister, VaultUpload, UploadTotp};
use crate::models::{User, NewUser};
use crate::enums::VaultUploadStatus;
use crate::enums::TotpUploadStatus;
use diesel::RunQueryDsl;
use crate::security::{hash_password, self};
use actix_web::web::Json;
use diesel::prelude::SqliteConnection;
use diesel::Connection;
use diesel::ExpressionMethods;
use diesel::TextExpressionMethods;
use diesel::QueryDsl;
use dotenv::dotenv;
use std::env;

pub fn connect_to_db() -> SqliteConnection {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("Database url must be set!");
    SqliteConnection::establish(&database_url).expect("Failed to connect")
}


// check if a user has already signed up with that email
fn search_for_email(search_email: String) -> bool {
    use crate::schema::users;
    let sql = connect_to_db();
    let query = users::table
        .filter(users::dsl::email.like(search_email))
        .load::<User>(&sql)
        .expect("Failed to query database");
    if query.len() != 0 {
        return true;
    } else {
        return false;
    }
}

fn register_user(email: String, password: String) -> crate::enums::UserCreateStatus {
    let sql = connect_to_db();
    let email_taken = search_for_email(email.clone());
    if email_taken {
        println!("Email already taken in database");
        return crate::enums::UserCreateStatus::EmailTaken;
    }
    let vault_raw = crate::vault::get_default_vault();
    let vault_encrypted = security::encrypt_data(password.clone(), vault_raw);
    let user = NewUser {
        email: &email,
        password_hashed: &hash_password(password),
        vault: &vault_encrypted
    };
    diesel::insert_into(crate::schema::users::table)
        .values(&user)
        .execute(&sql)
        .expect("Failed to update database");
    return crate::enums::UserCreateStatus::Created;
}

pub fn register_user_req(user: Json<UserRegister>) -> crate::enums::UserCreateStatus {
    println!("Creating new user in database from request");
    let user_email = &user.email;
    let user_passwd = &user.password;
    let stat = register_user(user_email.to_string(), user_passwd.to_string());
    return stat;
}

pub fn get_user_vault(email: String, password: String) -> Result<serde_json::Value, VaultReadStatus> {
    let sql = connect_to_db();
    use crate::schema::users;
    let query = users::table.filter(
        users::dsl::email.like(email))
        .load::<User>(&sql)
        .expect("Failed to query database");
    if query.len() == 0 {
        // return error
        return Err(VaultReadStatus::InvalidUser);
    } else {
        let user = &query[0];
        let user_password_hashed = &user.password_hashed;
        if !security::verify_password(password.clone(), user_password_hashed.to_string()) {
            return Err(VaultReadStatus::InvalidUser);
        }
        let vault_encrypted = &user.vault;
        let vault_decrypted = security::decrypt_data(password, vault_encrypted.to_string());
        // parse the vault json
        let vault_parsed = serde_json::from_str(&vault_decrypted).expect("Failed to parse vault json");
        return Ok(vault_parsed);
    }
}

pub fn get_user_vault_req(user: Json<UserRegister>) -> Result<serde_json::Value, VaultReadStatus> {
    let email = &user.email;
    let password = &user.password;
    let vault_status = get_user_vault(email.to_string(), password.to_string());
    return vault_status;
}

pub fn set_vault(password: String, email_check: String, vault_string: Value) -> VaultUploadStatus {
    // check password
    let sql = connect_to_db();
    use crate::schema::users::dsl::{users, id, vault};
    let query = crate::schema::users::table.filter(
        crate::schema::users::dsl::email.like(email_check))
        .load::<User>(&sql)
        .expect("Failed to query database");
    if query.len() == 0 {
       // TODO: return invalid user 
       return crate::enums::VaultUploadStatus::InvalidUser;
    } else {
        if !security::verify_password(password.clone(), query[0].password_hashed.clone()) {
            return VaultUploadStatus::InvalidUser;
        }
        // check if the vault is a string
        // encrypt the vault data
        let vault_encrypted = security::encrypt_data(password, vault_string.to_string());
        // set value in db
        let _ = diesel::update(users.filter(id.eq(query[0].id)))
            .set(vault.eq(&vault_encrypted))
            .execute(&sql)
            .expect("Failed to update database");

        return crate::enums::VaultUploadStatus::UploadedVault;
    }
}

pub fn set_vault_req(request: Json<VaultUpload>) -> VaultUploadStatus {
    let email = &request.email;
    let password = &request.password;
    let vault = request.vault_data.clone();
    return set_vault(password.to_string(), email.to_string(), vault);
}

pub fn add_totp_req(request: Json<UploadTotp>) -> TotpUploadStatus {
    let name = &request.totp_name;
    let email = &request.email;
    let password = &request.password;
   
    use crate::schema::users;
    let sql = connect_to_db();
        let query = users::table
        .filter(users::dsl::email.like(email))
        .load::<User>(&sql)
        .expect("Failed to query database");
    if query.len() == 0 {
        return TotpUploadStatus::InvalidUser;
    } else {
        // compare passwords
        if !security::verify_password(password.to_string(), query[0].password_hashed.to_string()) {
            return TotpUploadStatus::InvalidUser;
        } else {
            // get vault and decrypt it
            let vault_encrypted = &query[0].vault;
            let vault_decrypted = security::decrypt_data(password.to_string(), vault_encrypted.to_string());
            let mut vault_parsed: Value = serde_json::from_str(&vault_decrypted).expect("Failed to parse vault json");
            vault_parsed["items"].as_array_mut().unwrap().push(json!({
                "name": name,
                "id": uuid::Uuid::new_v4().to_string(),
                "type": "totp",
                "login": {
                    "secret": &request.totp_secret
                }
            }));
            let vault_set = set_vault(password.to_string(), email.to_string(), vault_parsed);
            match vault_set {
                VaultUploadStatus::InvalidUser => {
                    return TotpUploadStatus::InvalidUser;
                },
                VaultUploadStatus::UploadedVault => {
                    return TotpUploadStatus::UploadedTotp;
                }
            }
        }
    }
}
