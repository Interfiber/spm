use serde_json::json;
use std::time::{SystemTime, UNIX_EPOCH};
use totp_lite::{totp_custom, Sha1, DEFAULT_STEP};
use koibumi_base32 as base32;

pub fn get_default_vault() -> String {
    return json!({
        "items": [{
            "name": "Example",
            "type": "password",
            "id": "example_password",
            "login": {
                "username": "example",
                "password": "123",
                "website": "https://example.com"
            },
            "note": "this is an example password"
        }],
        "vault_version": 1
    }).to_string();
}


pub fn decode_totp(password: String) -> String {
     let seconds: u64 = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs();
     let result: String = totp_custom::<Sha1>(DEFAULT_STEP, 6, &base32::decode(password.trim().to_lowercase().to_string()).unwrap(), seconds);
     return result;
}
