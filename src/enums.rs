#[derive(PartialEq)]
pub enum UserCreateStatus {
    Created,
    EmailTaken
}

#[derive(PartialEq)]
pub enum VaultReadStatus {
    InvalidUser
}

#[derive(PartialEq)]
pub enum VaultUploadStatus {
    InvalidUser,
    UploadedVault
}

#[derive(PartialEq)]
pub enum TotpUploadStatus {
    InvalidUser,
    UploadedTotp
}
