use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct UserRegister {
    pub email: String,
    pub password: String
}

#[derive(Deserialize)]
pub struct VaultUpload {
    pub email: String,
    pub password: String,
    pub vault_data: serde_json::Value
}

#[derive(Deserialize)]
pub struct UploadTotp {
    pub email: String,
    pub password: String,
    pub totp_name: String,
    pub totp_secret: String
}

#[derive(Deserialize)]
pub struct DecodeTotp {
    pub totp_secret: String
}

#[derive(Serialize)]
pub struct UserCreate {
    pub error: bool,
    pub msg: String
}

