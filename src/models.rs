use diesel::Queryable;
use super::schema::users;
use diesel::Insertable;

#[derive(Queryable, Debug)]
pub struct User {
    pub id: i32,
    pub email: String,
    pub password_hashed: String,
    pub vault: String
}

#[derive(Insertable)]
#[table_name="users"]
pub struct NewUser<'a> {
    pub email: &'a str,
    pub password_hashed: &'a str,
    pub vault: &'a str
}
