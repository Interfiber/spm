# SPM
Simple Password Manager

## About
SPM is a simple web-based password manager, built to be lightweight on server resources, and
secure. SPM ultra-fast, built in rust, and uses minimal client-side javascript.

## Installation
Installing SPM

### With Docker
TODO

### Manually
```bash
git clone https://gitlab.com/Interfiber/spm.git
cd spm
# nix-shell 
cargo build --release
# update .env if needed
# install diesel client
diesel migration run # create database
./target/release/spm # opens on port 8080
```

### Cross-Compile for Armv7
First spin up a docker container with ubuntu:bionic and run bash in it then run the following command inside the container:
```bash
apt update && apt install curl
curl -#L https://gitlab.com/Interfiber/spm/-/raw/main/crosscompile-armv7-ubuntu.sh | bash
```

## TOTP support
Time-Based-One-Time-Passwords are supported.
